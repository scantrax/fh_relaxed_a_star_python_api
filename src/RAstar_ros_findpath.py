#!/usr/bin/env python

#findpath needs to be a service (action better but mucho difficulto) called by RAstarPlanner function in RAstar_ros.cpp
#Inputs: the map layout, the start and the goal Cells and a boolean to indicate if we will use break ties or not
#Output: the best path
#Description: it is used to generate the robot free path
#*********************************************************************************#
import rospy
import sys

from fh_relaxed_a_star_python_api.srv import findpath
from fh_relaxed_a_star_python_api.srv import *

def cp_sort(a,b):
    if a["fCost"] > b["fCost"]:
        return 1
    elif a["fCost"] == b["fCost"]:
        return 0
    else:
        return -1

def handle_findpath(req):
        emptyPath = []     
        calculatehCost = 0
        print("Calculating best path to goal...")
        CP = {'currentcell': 0,'fCost': 0.}

        g_score = list(req.g_score)

        print req.startcell
        g_score[req.startcell] = 0
        CP['currentcell'] = req.startcell
        
        ##waiting and initializing service call for calculateHCost
        print"Waiting for calculateHCost service"
        rospy.wait_for_service('Nodehandle_for_FH_RAstar_Service_Calls/service_calculateHCost')
        calculatehCost = rospy.ServiceProxy('Nodehandle_for_FH_RAstar_Service_Calls/service_calculateHCost',calculateHCost) #calculateHCost is the srv message name
        CP['fCost'] = g_score[req.startcell]#+calculatehCost(startCell, goalCell)
        print"calculateHCost done"
        
        OPL = []
        OPL.append(CP)
        currentcell = req.startcell

        ##waiting and initializing service call for calculateHCost
        rospy.wait_for_service('Nodehandle_for_FH_RAstar_Service_Calls/service_findFreeNeighborCell')
        findFreeNeighborCells = rospy.ServiceProxy('Nodehandle_for_FH_RAstar_Service_Calls/service_findFreeNeighborCell',freeneighborCells)
        
        ##waiting and initializing service call for getMoveCost
        rospy.wait_for_service('Nodehandle_for_FH_RAstar_Service_Calls/service_getMoveCost')
        getmovecost = rospy.ServiceProxy('Nodehandle_for_FH_RAstar_Service_Calls/service_getMoveCost',getmoveCost)

        ##waiting and initializing service call for constructPath
        rospy.wait_for_service('Nodehandle_for_FH_RAstar_Service_Calls/service_constructPath')
        constructpath = rospy.ServiceProxy('Nodehandle_for_FH_RAstar_Service_Calls/service_constructPath',constructPath)

        print "Calculating best path..."
        print "getMoveCost calculation..."

        while OPL != [] and g_score[req.goalcell] == float("inf"):
            OPL.sort(cp_sort)
            #print g_score[req.goalcell]
            currentcell = OPL.pop(0)["currentcell"]

            resp = findFreeNeighborCells(currentcell)
            neighborcells = resp.freeNeighborCells

           # print len(neighborcells)-1
            
            #print "goalcell: " + str(req.goalcell)
            for i in neighborcells:
                #print "i: " + str(i)
                if i == req.goalcell:
                    print "hier ist sie!"
                if g_score[i]== float("inf"):
                   g_score[i] = g_score[currentcell] + getmovecost(currentcell,i).movecost
                   CP['currentcell'] = i
                   CP['fCost'] = g_score[i]+calculatehCost(i,req.goalcell).cellDistance
                   OPL.append(CP)
                   #print"g_score[neighborcells[" +str(i)+ "] = " +str(g_score[i])

        print "Calculation done!"
        print g_score[req.goalcell]

        if g_score[req.goalcell]!= float("inf"):
           
            print "Constructing of path..."
            print "in construct service"
                        
            bestPath = constructpath(req.startcell, req.goalcell,g_score).bestPath
            rospy.loginfo("Path found!")
            return findpathResponse(bestPath)

        else:
            rospy.logerr("Failure to find path!!")
            return findpathResponse(emptyPath)

def RAstar_ros_find_path_server():
    rospy.init_node('RAstar_ros_find_path_server')
    s = rospy.Service('service_findpath', findpath, handle_findpath)
    print "Findpath_Service ready"
    rospy.spin()

if __name__ == "__main__":
    RAstar_ros_find_path_server()